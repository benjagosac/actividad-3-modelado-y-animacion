using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigLata : MonoBehaviour
{
    public Animator Lata;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")){
            Lata.Play("Lata");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player")){
            Lata.Play("New State");
        }
    }
}
