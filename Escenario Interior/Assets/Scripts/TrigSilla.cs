using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrigSilla : MonoBehaviour
{
    public Animator Silla;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")){
            Silla.Play("SillaAzul");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player")){
            Silla.Play("regresa");
        }
    }
}
